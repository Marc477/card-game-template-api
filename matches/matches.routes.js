const MatchesController = require('./matches.controller');
const MatchesTool = require('./matches.tool');
const AuthTool = require('../authorization/auth.tool');
const config = require('../config');

const ADMIN = config.permissionLevels.ADMIN; //Highest permision, can read and write all users
const SERVER = config.permissionLevels.SERVER; //Higher permission, can read all users
const USER = config.permissionLevels.USER; //Lowest permision, can only do things on same user

exports.route = function (app) {

    app.post('/matches/add', app.post_limiter, [
        AuthTool.isValidJWT,
        AuthTool.isPermissionLevel(SERVER),
        MatchesController.addMatch
    ]);
    app.post('/matches/complete', app.post_limiter, [
        AuthTool.isValidJWT,
        AuthTool.isPermissionLevel(SERVER),
        MatchesController.completeMatch
    ]);

    //-- Getter
    app.get('/matches', [
        AuthTool.isValidJWT,
        AuthTool.isPermissionLevel(SERVER),
        MatchesController.getAll
    ]);
    app.get('/matches/:tid', [
        AuthTool.isValidJWT,
        AuthTool.isPermissionLevel(USER),
        MatchesController.getByTid
    ]);

    //---- Stats ------
    app.get('/matches/stats/cards', [
        AuthTool.isValidJWT,
        AuthTool.isPermissionLevel(USER),
        MatchesController.getCardStats
    ]);
    app.get('/matches/stats/cards/:date', [
        AuthTool.isValidJWT,
        AuthTool.isPermissionLevel(USER),
        MatchesController.getCardStats
    ]);
    app.get('/matches/stats/cards/:date/:elo_min/:elo_max', [
        AuthTool.isValidJWT,
        AuthTool.isPermissionLevel(USER),
        MatchesController.getCardStats
    ]);
    app.get('/matches/stats/artefacts', [
        AuthTool.isValidJWT,
        AuthTool.isPermissionLevel(USER),
        MatchesController.getArtefactStats
    ]);
    app.get('/matches/stats/artefacts/:date', [
        AuthTool.isValidJWT,
        AuthTool.isPermissionLevel(USER),
        MatchesController.getArtefactStats
    ]);
    app.get('/matches/stats/artefacts/:date/:elo_min/:elo_max', [
        AuthTool.isValidJWT,
        AuthTool.isPermissionLevel(USER),
        MatchesController.getArtefactStats
    ]);

    
    
    /*app.get('/matches/latest/:userId', [
        AuthTool.isValidJWT,
        AuthTool.isPermissionLevel(USER),
        MatchesController.getLatest
    ]);*/
    /*app.get('/matches/daily/:userId/:date', [
        AuthTool.isValidJWT,
        AuthTool.isPermissionLevel(USER),
        MatchesController.getDaily
    ]);
    app.get('/matches/dailywin/:userId/:date', [
        AuthTool.isValidJWT,
        AuthTool.isPermissionLevel(USER),
        MatchesController.getDailyWin
    ]);*/
};
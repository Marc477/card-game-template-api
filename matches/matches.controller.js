const MatchModel = require('./matches.model');
const MatchTool = require('./matches.tool');
const UserModel = require('../users/users.model');
const UserTool = require('../users/users.tool');
const DateTool = require('../tools/date.tool');
const Validator = require('../tools/validator.tool');
const config = require('../config');


exports.addMatch = async(req, res) => {
    
    var tid = req.body.tid;
    var players = req.body.players;
    var decks = req.body.decks;
    var ranked = req.body.ranked ? true : false;
    var mode = req.body.mode || "";
    var gems = req.body.gems || 0;

    if (!tid || !players || !Array.isArray(players) || players.length != 2)
        return res.status(400).send({ error: "Invalid parameters" });

    if (!decks || !Array.isArray(decks) || decks.length != 2)
        return res.status(400).send({ error: "Invalid parameters" });

    if (mode && typeof mode !== "string")
        return res.status(400).send({ error: "Invalid parameters" });

    if (gems && !Number.isInteger(gems))
        return res.status(400).send({ error: "Invalid parameters" });

    var fmatch = await MatchModel.getByTid(tid);
    if(fmatch)
        return res.status(400).send({error:"Match already exists: " + tid});

    var player0 = await UserModel.getByUsername(players[0]);
    var player1 = await UserModel.getByUsername(players[1]);
    if(!player0 || !player1)
        return res.status(404).send({error:"Can't find players"});

    if(player0.id == player1.id)
        return res.status(400).send({error:"Can't play against yourself"});

    var match = {};
    match.tid = tid;
    match.players = players;
    match.winner = "";
    match.completed = false;
    match.ranked = ranked;
    match.mode = mode;
    match.gems = gems;
    match.start = Date.now();
    match.end = Date.now();
    match.udata = [];
    match.udata.push(GetPlayerData(player0, decks[0]));
    match.udata.push(GetPlayerData(player1, decks[1]));

    var match = await MatchModel.create(match);
    if(!match)
        return res.status(500).send({error:"Unable to create match"});

    res.status(200).send(match);

};

exports.completeMatch = async(req, res) => {
    
    var matchId = req.body.tid;
    var winner = req.body.winner;

    if (!matchId || !winner)
        return res.status(400).send({ error: "Invalid parameters" });
	
	if(typeof matchId != "string" || typeof winner != "string")
        return res.status(400).send({error: "Invalid parameters" });

    var match = await MatchModel.getByTid(matchId);
    if(!match)
        return res.status(404).send({error: "Match not found"});

    if(match.completed)
        return res.status(200).send(match);

    var player0 = await UserModel.getByUsername(match.players[0]);
    var player1 = await UserModel.getByUsername(match.players[1]);
    if(!player0 || !player1)
        return res.status(404).send({error:"Can't find players"});

    match.end = Date.now();
    match.winner = winner;
    match.completed = true;
    
    if(match.ranked)
    {
        var prev_elo0 = player0.rank;
        var prev_elo1 = player1.rank;

        var reward0 = await GainMatchReward(player0, player1, prev_elo0, prev_elo1, winner);
        var reward1 = await GainMatchReward(player1, player0, prev_elo1, prev_elo0, winner);

        match.udata[0].reward = reward0;
        match.udata[1].reward = reward1;
        match.markModified('udata');
    }

    //Save match
    var uMatch = await match.save();
    
    //Return
    res.status(200).send(uMatch);
};

var GetPlayerData = (player, deck_tid) => 
{
    var data = {};
    data.username = player.username;
    data.rank = player.rank;
    data.deck = UserTool.getDeck(player, deck_tid);
    data.reward = {};
    return data;
}

var GainMatchReward = async(player, opponent, player_elo, opponent_elo, winner_username) => {

    var won = winner_username == player.username;
    var lost = winner_username == opponent.username;

    //Rewards
    var xp = won ? config.xp_victoire : config.xp_defeat;
    var credits = won ? config.credits_victory : config.credits_defeat;

    player.xp += xp;
    player.credits += credits;

    //Match winrate
    player.matches +=1;

    if(won)
        player.victories += 1;
    else if (lost)
        player.defeats += 1;
    
    //Calculate elo
    var match_count = player.matches || 0;
    var match_progress = Math.min(Math.max(match_count / config.elo_ini_match, 0.0), 1.0);
    var new_rank = MatchTool.calculateELO(player_elo, opponent_elo, match_progress, won, lost);
    player.rank = new_rank;
    player.save();

    var reward = {
        rank: player.rank,
        xp: xp,
        credits: credits
    }; 

    return reward;
};

exports.getAll = async(req, res) => {

    var start =  req.query.start ? DateTool.tagToDate(req.query.start) : null;
    var end =  req.query.end ? DateTool.tagToDate(req.query.end) : null;

    var matches = await MatchModel.list(start, end);
    if(!matches)
        return res.status(400).send({error: "Invalid Parameters"});

    return res.status(200).send(matches);
};

exports.getByTid = async(req, res) => {

    var match = await MatchModel.getByTid(req.params.tid);
    if(!match)
        return res.status(404).send({error: "Match not found " + req.params.tid});
    
    return res.status(200).send(match);
};

exports.getLatest = async(req, res) => {

    var match = await MatchModel.getLast(req.params.userId);
    if(!match)
        return res.status(404).send({error: "Match not found for user " + req.params.userId});
    
    return res.status(200).send(match);
};

exports.getArtefactStats = async(req, res) =>
{
    var date = DateTool.tagToDate(req.params.date) || DateTool.minDate();
    var elo_min = Number.parseInt(req.params.elo_min) || 0;
    var elo_max = Number.parseInt(req.params.elo_max) || 99999;

    if(elo_min && !Number.isInteger(elo_min))
        return res.status(400).send({error: "Invalid Parameters"});
    
    if(elo_max && !Number.isInteger(elo_max))
        return res.status(400).send({error: "Invalid Parameters"});

    if(date && !DateTool.isDate(date))
        return res.status(400).send({error: "Invalid Parameters"});

    var matches = await MatchModel.list(date, null, null, true);
    if(!matches)
        return res.status(400).send({error: "Invalid Parameters"});

    var artefacts = new Map();

    matches.forEach((match, index) =>
    {
        if(match.ranked)
        {
            var data0 = match.udata[0];
            var data1 = match.udata[1];
            var elo = (data0.rank + data1.rank) / 2;
            var match_date = new Date(match.end);
            if(elo >= elo_min && elo <= elo_max && match_date >= date)
            {
                AddArtefactStats(artefacts, data0.deck, data0.username == match.winner);
                AddArtefactStats(artefacts, data1.deck, data1.username == match.winner);
            }
        }
    });

    var array = Array.from(artefacts, ([name, value]) => (value));
    array.forEach((artefact, index) =>
    {
        artefact.win_rate = artefact.matches > 0 ? (artefact.wins * 100.0 / artefact.matches) : 0.0;
        artefact.win_rate = Math.round(artefact.win_rate * 100.0) / 100.0;
    });

    array.sort( (a, b) =>
    {
        if(a.win_rate == b.win_rate && a.matches == b.matches)
            return a.tid.localeCompare(b.tid);
        if(a.win_rate == b.win_rate)
            return a.matches > b.matches ? -1 : 1;
        return a.win_rate > b.win_rate ? -1 : 1;
    });

    return res.status(200).send(array);
}

var AddArtefactStats = function(artefacts, deck, win)
{
    if(deck && deck.artefact)
    {
        var data;
        if(artefacts.has(deck.artefact))
        {
            data = artefacts.get(deck.artefact);
        }
        else
        {
            data = {
                artefact: deck.artefact,
                matches: 0,
                wins: 0,
            };
        }
        
        data.matches += 1;
        if(win)
            data.wins += 1;

        artefacts.set(deck.artefact, data);
    }
}


exports.getCardStats = async(req, res) =>
{
    var date = DateTool.tagToDate(req.params.date) || DateTool.minDate();
    var elo_min = Number.parseInt(req.params.elo_min) || 0;
    var elo_max = Number.parseInt(req.params.elo_max) || 99999;

    if(elo_min && !Number.isInteger(elo_min))
        return res.status(400).send({error: "Invalid Parameters"});
    
    if(elo_max && !Number.isInteger(elo_max))
        return res.status(400).send({error: "Invalid Parameters"});

    if(date && !DateTool.isDate(date))
        return res.status(400).send({error: "Invalid Parameters"});

    var matches = await MatchModel.list(date, null, null, true);
    if(!matches)
        return res.status(400).send({error: "Invalid Parameters"});

    var cards = new Map();

    matches.forEach((match, index) =>
    {
        if(match.ranked)
        {
            var data0 = match.udata[0];
            var data1 = match.udata[1];
            var elo = (data0.rank + data1.rank) / 2;
            var match_date = new Date(match.end);
            if(elo >= elo_min && elo <= elo_max && match_date >= date)
            {
                AddDeckStats(cards, data0.deck, data0.username == match.winner);
                AddDeckStats(cards, data1.deck, data1.username == match.winner);
            }
        }
    });

    var array = Array.from(cards, ([name, value]) => (value));
    array.forEach((card, index) =>
    {
        card.win_rate = card.matches > 0 ? (card.wins * 100.0 / card.matches) : 0.0;
        card.win_rate = Math.round(card.win_rate * 100.0) / 100.0;

        delete card.tempus;
        delete card.domination;
        delete card.rush;
    });

    array.sort( (a, b) =>
    {
        if(a.win_rate == b.win_rate && a.matches == b.matches)
            return a.tid.localeCompare(b.tid);
        if(a.win_rate == b.win_rate)
            return a.matches > b.matches ? -1 : 1;
        return a.win_rate > b.win_rate ? -1 : 1;
    });

    return res.status(200).send(array);
}

var AddDeckStats = function(cards, deck, win)
{
    if(deck && Array.isArray(deck.cards))
    {
        deck.cards.forEach((card, index) =>
        {
            AddCardStats(cards, card, win, deck.artefact)
        });
        
    }
}

var AddCardStats = function(cards, card, win, artefact)
{
    if(card)
    {
        if(card.endsWith("_foil"))
            card = card.replace("_foil", "");
        if(card.endsWith("_nft"))
            card = card.replace("_nft", "");

        var data;
        if(cards.has(card))
        {
            data = cards.get(card);
        }
        else
        {
            data = {
                tid: card,
                matches: 0,
                wins: 0,
                domination: 0,
                rush: 0,
                tempus: 0,
            };
        }
        
        data[artefact] += 1;
        data.matches += 1;
        if(win)
            data.wins += 1;

        cards.set(card, data);
    }
}


/*exports.getDaily = (req, res) => {

    var start =  DateTool.getStartOfDayUTC(req.params.date);
    var end =  DateTool.getEndOfDayUTC(req.params.date);

    MatchModel.list(start, end, req.params.userId)
        .then((result) => {
            var promises = [];
            result.forEach(function(r){ 
                promises.push(Promise.resolve(r)); 
            });
            Promise.all(promises).then(function(){
                return res.status(200).send(result);
            }).catch(function(error){
                return res.status(404).send({error: "Matches not found"});
            });
        })
        .catch((error) =>{
            return res.status(400).send({error: "Invalid Parameters"});
        });
};


exports.getDailyWin = (req, res) => {

    var start =  DateTool.getStartOfDayUTC(req.params.date);
    var end =  DateTool.getEndOfDayUTC(req.params.date);

    MatchModel.list(start, end, req.params.userId, true)
        .then((result) => {
            var promises = [];
            result.forEach(function(r){ 
                promises.push(Promise.resolve(r)); 
            });
            Promise.all(promises).then(function(){
                return res.status(200).send(result);
            }).catch(function(error){
                return res.status(404).send({error: "Matches not found"});
            });
        })
        .catch((error) =>{
            return res.status(400).send({error: "Invalid Parameters"});
        });
};*/

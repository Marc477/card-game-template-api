const MatchModel = require('./matches.model');
const crypto = require('crypto');
const config = require('../config.js');
const Validator = require('../tools/validator.tool');
const DateTool = require('../tools/date.tool');

exports.calculateELO = (player_elo, opponent_elo, progress, won, lost) =>
{
    var p_elo = player_elo || 1000;
    var o_elo = opponent_elo || 1000;

    var p_elo_log = Math.pow(10.0, p_elo / 400.0);
    var o_elo_log = Math.pow(10.0,  o_elo / 400.0);
    var p_expected = p_elo_log / (p_elo_log + o_elo_log);
    var p_score = won ? 1.0 : (lost ? 0.0 : 0.5);
    
    progress = Math.min(Math.max(progress, 0.0), 1.0);
    var elo_k = progress * config.elo_k + (1.0 - progress) * config.elo_ini_k;
    var new_elo = Math.round(p_elo + elo_k * (p_score - p_expected));
    return new_elo;
}
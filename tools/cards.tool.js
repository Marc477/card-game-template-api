const config = require('../config.js');

const CardTool = exports;

exports.getCommonCards = function()
{
    return config.cards_common_earth.concat(config.cards_common_proxima).concat(config.cards_common_elysia);
}

exports.getUncommonCards = function()
{
    return config.cards_uncommon_earth.concat(config.cards_uncommon_proxima).concat(config.cards_uncommon_elysia);
}

exports.getRareCards = function()
{
    return config.cards_rare_earth.concat(config.cards_rare_proxima).concat(config.cards_rare_elysia);
}

exports.getMythicCards = function()
{
    return config.cards_mythic_earth.concat(config.cards_mythic_proxima).concat(config.cards_mythic_elysia);
}
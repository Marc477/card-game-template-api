const schedule = require('node-schedule');
const UserTool = require('../users/users.tool');

const ExecuteJobs = function()
{
    console.log('Run Hourly Jobs.....');

    //Clear Register
    UserTool.clearRegisterSessions();
};

exports.InitJobs = function()
{
    schedule.scheduleJob('* 1 * * *', function(){  // this for one hour
        ExecuteJobs();
    });
    
    //Test run when starting
    ExecuteJobs();
}
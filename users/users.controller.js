const UserModel = require('./users.model');
const UserTool = require('./users.tool');
const DateTool = require('../tools/date.tool');
const Activity = require("../activity/activity.model");
const Validator = require('../tools/validator.tool');
const Email = require('../tools/email.tool');
const crypto = require('crypto');
const config = require('../config');

//Register new user
exports.registerUser = async (req, res, next) => {

	if(!req.body.email || !req.body.username || !req.body.password){
        return res.status(400).send({error: 'Invalid parameters'});
    }

    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;
    var avatar = req.body.avatar || "";

    if(!Validator.validateUsername(username)){
        return res.status(400).send({error: 'Invalid username'});
    }

    if(!Validator.validateEmail(email)){
        return res.status(400).send({error: 'Invalid email'});
    }

    if(!Validator.validatePassword(password)){
        return res.status(400).send({error: 'Invalid password'});
    }

    if(avatar && typeof avatar !== "string")
        return res.status(400).json({error: "Invalid avatar"});

    var user_username = await UserModel.getByUsername(username);
    var user_email = await UserModel.getByEmail(email);

    if(user_username)
        return res.status(400).send({error: 'Username already exists'});
    if(user_email)
        return res.status(400).send({error: 'Email already exists'});
	
    let user = {};

    user.username = req.body.username;
    user.email = req.body.email;
    user.permissionLevel = 1;
    user.validationLevel = 0;
    user.avatar = req.body.avatar || "";
    user.quote = req.body.quote || "";

    user.accountCreationTime = new Date();
    user.lastLoginTime = new Date();
    user.refreshKey = "";
    user.emailConfirmKey = UserTool.generateID(20);
    user.passwordRecoveryCode = UserTool.generateID(10, true);
	
	UserTool.setUserPassword(user, password);

    user.credits = 1000;
    user.xp = 0;
    user.rank = 1000;

    user.matches = 0;
    user.victories = 0;
    user.defeats = 0;

    user.ship = null;
    user.cardback = "cardback1";
    user.trophy = "";

    user.cards = [];
    user.decks = [];
    user.avatars = [];
    user.cardbacks = [];
    user.rewards = [];

    if(user.avatar)
        user.avatars.push(user.avatar);
    if(user.cardback)
        user.cardbacks.push(user.cardback);

    //Create user
    var nUser = await UserModel.createUser(user);
    if(!nUser)
        return res.status(500).send({ error: "Unable to create user" });
    
    //Send confirm email
    UserTool.sendEmailConfirmKey(nUser, user.email, user.emailConfirmKey);

    // Activity Log -------------
    const activityData = {username: user.username, email: user.email };
    const a = await Activity.LogActivity("register", user.username, activityData);
    if (!a) return res.status(500).send({ error: "Failed to log activity!!" });

    //Add register session
    UserTool.addRegisterSession(req.ip);
    
    //Return response
    return res.status(200).send({ success: true, id: nUser._id });
};

exports.getAll = async(req, res) => {

    let user_permission_level = parseInt(req.jwt.permissionLevel);
    let is_admin = (user_permission_level >= config.permissionLevels.SERVER);
    
    var list = await UserModel.list();
    for(var i=0; i<list.length; i++){
        if(is_admin)
            list[i] = list[i].deleteSecrets();
        else
            list[i] = list[i].deleteAdminOnly();
    }

    return res.status(200).send(list);
};

exports.getUser = async(req, res) => {
    var user = await UserModel.getById(req.params.userId);
    if(!user)
        user = await UserModel.getByUsername(req.params.userId);

    if(!user)
        return res.status(404).send({error: "User not found " + req.params.userId});
    
    let user_permission_level = parseInt(req.jwt.permissionLevel);    
    let is_admin = (user_permission_level >= config.permissionLevels.SERVER);
    if(is_admin || req.params.userId == req.jwt.userId || req.params.userId == req.jwt.username)
        user = user.deleteSecrets();
    else
        user = user.deleteAdminOnly();

    user.serverTime = new Date(); //Return server time
    return res.status(200).send(user);
};

exports.patchUserAccount = async(req, res) => {

    var userId = req.params.userId;
    var avatar = req.body.avatar;
    var cardback = req.body.cardback;
    var trophy = req.body.trophy;
    var quote = req.body.quote;

    if(!userId || typeof userId !== "string")
        return res.status(400).json({error: "Invalid parameters"});

    if(avatar && typeof avatar !== "string")
        return res.status(400).json({error: "Invalid avatar"});

    if(cardback && typeof cardback !== "string")
        return res.status(400).json({error: "Invalid avatar"});
    
    if(trophy && typeof trophy !== "string")
        return res.status(400).json({error: "Invalid trophy"});

    if(quote && typeof quote !== "string")
        return res.status(400).json({error: "Invalid quote"});
    
    var user = await UserModel.getById(userId);
    if(!user)
        return res.status(404).send({error: "User not found: " + userId});

    var userData = {};

    //Change avatar
    if(avatar && avatar.length < 50)
        userData.avatar = avatar;

    if(cardback && cardback.length < 50)
        userData.cardback = cardback;

    if(trophy && trophy.length < 50)
        userData.trophy = trophy;

    //Change quote
    if(quote && quote.length < 500)
        userData.quote = quote;

    //Update user
    var result = await UserModel.update(user, userData);
    if(!result)
        return res.status(400).send({error: "Error updating user: " + userId});

    return res.status(200).send(result.deleteAdminOnly());
};

exports.patchEmail = async(req, res) => {

    var userId = req.jwt.userId;
    var email = req.body.email;

    if(!userId || typeof userId !== "string")
        return res.status(400).json({error: "Invalid parameters"});

    if(!email || !Validator.validateEmail(email))
        return res.status(400).json({error: "Invalid email"});

    var user = await UserModel.getById(userId);
    if(!user)
        return res.status(404).send({error: "User not found: " + userId});

    if(email == user.email)
        return res.status(400).send({error: "Email unchanged"});

    //Find email
    var foundUserEmail = await UserModel.getByEmail(email);
    if(foundUserEmail)
        return res.status(403).json({error: "Email already exists"});

    var prev_email = user.email;
    var userData = {};
    userData.email = email;
    userData.validationLevel = 0;
    userData.emailConfirmKey = UserTool.generateID(20);
    
    //Update user
    var result = await UserModel.update(user, userData);
    if(!result)
        return res.status(400).send({error: "Error updating user email: " + userId});

    //Send confirmation email
    UserTool.sendEmailConfirmKey(user, email, userData.emailConfirmKey);
    UserTool.sendEmailChangeEmail(user, prev_email, email);

    // Activity Log -------------
    const activityData = {prev_email: prev_email, new_email: email };
    const a = await Activity.LogActivity("edit_email", req.jwt.username, {activityData});
    if (!a) return res.status(500).send({ error: "Failed to log activity!!" });

    return res.status(200).send(result.deleteAdminOnly());
};

exports.patchPassword = async(req, res) => {

    var userId = req.jwt.userId;
    var password = req.body.password;
    var password_previous = req.body.password_previous;

    if(!userId || typeof userId !== "string")
        return res.status(400).json({error: "Invalid parameters"});

    if(!password || !password_previous || typeof password !== "string" || typeof password_previous !== "string")
        return res.status(400).json({error: "Invalid parameters"});
    
    var user = await UserModel.getById(userId);
    if(!user)
        return res.status(404).send({error: "User not found: " + userId});

    let validPass = AuthTool.validatePassword(user, password_previous);
    if(!validPass)
        return res.status(401).json({error: "Invalid previous password"});

    UserTool.setUserPassword(user, password);

    var result = await UserModel.save(user, ["password", "refreshKey", "passwordRecoveryKey"]);
    if(!result)
        return res.status(500).send({error: "Error updating user password: " + userId});

    //Send confirmation email
    UserTool.sendEmailChangePassword(user, user.email);

    // Activity Log -------------
    const a = await Activity.LogActivity("edit_password", req.jwt.username, {});
    if (!a) return res.status(500).send({ error: "Failed to log activity!!" });

    return res.status(204).send({});
};


exports.resetPassword = async(req, res) => {

    var email = req.body.email;

    if(!email || typeof email !== "string")
        return res.status(400).json({error: "Invalid parameters"});

    var user = await UserModel.getByEmail(email);
    if(!user)
        return res.status(404).send({error: "User not found: " + email});

    user.passwordRecoveryKey = UserTool.generateID(10, true);
    await UserModel.save(user, ["passwordRecoveryKey"]);

    UserTool.sendEmailPasswordRecovery(user, email);

    return res.status(204).send({});
};


exports.resetPasswordConfirm = async(req, res) => {

    var email = req.body.email;
    var code = req.body.code;
    var password = req.body.password;

    if(!email || typeof email !== "string")
        return res.status(400).json({error: "Invalid parameters"});

    if(!code || typeof code !== "string")
        return res.status(400).json({error: "Invalid parameters"});

    if(!password || typeof password !== "string")
        return res.status(400).json({error: "Invalid parameters"});
    
    var user = await UserModel.getByEmail(email);
    if(!user)
        return res.status(404).send({error: "User not found: " + email});

    if(!user.passwordRecoveryKey || user.passwordRecoveryKey.toUpperCase() != code)
        return res.status(403).json({error: "Invalid Recovery Code"});
    
    UserTool.setUserPassword(user, password);

    var result = await UserModel.save(user, ["password", "refreshKey", "passwordRecoveryKey"]);
    if(!result)
        return res.status(500).send({error: "Error updating user password: " + email});

    return res.status(204).send({});
};

exports.removeById = async(req, res) => {
    UserModel.removeById(req.params.userId);
    return res.status(204).send({});
};

//In this function all message are returned in direct text because the email link is accessed from browser
exports.confirmEmail = async (req, res) =>{

    if(!req.params.userId || !req.params.emailCode){
        return res.status(404).send("Code invalid");
    }

    var user = await UserModel.getById(req.params.userId);
    if(!user)
        return res.status(404).send("Code invalid");

    if(user.emailConfirmKey != req.params.emailCode)
        return res.status(404).send("Code invalid");

    if(user.emailConfirmed && user.validationLevel >= 1)
        return res.status(400).send("Email already confirmed!");

    //Code valid!
    var data = {emailConfirmed: true, validationLevel: Math.max(user.validationLevel, 1)};
    UserModel.update(user, data)
    .then((result) => {
        return res.status(200).send("Email confirmed!");
    }).catch(() => {
        return res.status(404).send("Code invalid");
    });
};

exports.resendEmail = async(req, res) => 
{
    var userId = req.jwt.userId;
    var user = await UserModel.getById(userId);
    if(!user)
        return res.status(404).send({error: "User not found " + userId});

    if(user.emailConfirmed)
        return res.status(403).send({error: "Email already confirmed"});

    UserTool.sendEmailConfirmKey(user, user.email, user.emailConfirmKey);
    return res.status(200).send();
}

exports.sendEmail = async (req, res) =>{

    var subject = req.body.title;
    var text = req.body.text;
    var email = req.body.email;

    if(!subject || typeof subject !== "string")
        return res.status(400).json({error: "Invalid parameters"});

    if(!text || typeof text !== "string")
        return res.status(400).json({error: "Invalid parameters"});

    if(!email || typeof email !== "string")
        return res.status(400).json({error: "Invalid parameters"});

    Email.SendEmail(email, subject, text, function(result){
        console.log("Sent email to: " + email + ": " + result);
        return res.status(200).send({success: result});
    });
};

exports.getOnline = async(req, res) =>
{
    //Count online users
    var time = new Date();
    time = DateTool.addMinutes(time, -10);

    var count = 0;
    var users = await UserModel.list();
    var usernames = [];
    for(var i=0; i<users.length; i++)
    {
        var user = users[i];
        if(user.lastLoginTime > time)
        {
            usernames.push(user.username);
            count++;
        }
    }
    return res.status(200).send({online: count, total: users.length, users: usernames});
};
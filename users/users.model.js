const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
	
  username: {type: String, required: true, index: true, unique: true, default: ""},
  email: {type: String, required: true, index: true, default: ""},
  password: {type: String, required: true, default: ""},
  
  permissionLevel: {type: Number, required: true, default: 1},
  validationLevel: {type: Number, required: true, default: 0},
  accountCreationTime: {type: Date, default: null},
  lastLoginTime: {type: Date, default: null},
  
  refreshKey: {type: String, default: ""},           //Used for refreshing the current login JWT token
  proofKey: {type: String, default: ""},             //Used to proof to a another server who you are
  emailConfirmKey: {type: String, default: ""},      //Used to confirm email
  passwordRecoveryKey: {type: String, default: ""},  //Used for password recovery   

  avatar: {type: String, default: ""},
  quote: {type: String, default: ""},

  credits: {type: Number, default: 0},
  xp: {type: Number, default: 0},
  rank: {type: Number, default: 1000},

  matches: {type: Number, default: 0},
  victories: {type: Number, default: 0},
  defeats: {type: Number, default: 0},

  cardback: {type: String, default: ""},
  friends: [{type: String}],

  cards: [{ type: Object, _id: false }],
  decks: [{ type: Object, _id: false }],
  
  avatars: [{type: String}],
  cardbacks: [{type: String}],
  rewards: [{type: String}],
  

});

userSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

userSchema.methods.toObj = function() {
    var user = this.toObject();
    user.id = user._id;
    delete user.__v;
    delete user._id;
    return user;
};

userSchema.methods.deleteSecrets = function(){
    var user = this.toObject();
    user.id = user._id;
    delete user.__v;
    delete user._id;
	delete user.permissionLevel;
    delete user.validationLevel;
    delete user.password;
    delete user.refreshKey; 
    delete user.proofKey; 
    delete user.emailConfirmKey; 
    delete user.passwordRecoveryKey;
    return user;
};

userSchema.methods.deleteAdminOnly = function(){
    var user = this.toObject();
    delete user.__v;
    delete user._id;
    delete user.email;
    delete user.permissionLevel;
    delete user.validationLevel;
    delete user.password; 
    delete user.refreshKey; 
    delete user.proofKey;
    delete user.emailConfirmKey; 
    delete user.passwordRecoveryKey;
    return user;
};

const User = mongoose.model('Users', userSchema);
exports.UserModel = User;

// USER DATA MODELS ------------------------------------------------

exports.getByEmail = async(email) => {

    try{
        var regex = new RegExp(["^", email, "$"].join(""), "i");
        var user = await User.findOne({email: regex});
        return user;
    }
    catch{
        return null;
    }
};

exports.getByUsername = async(username) => {

    try{
        var regex = new RegExp(["^", username, "$"].join(""), "i");
        var user = await User.findOne({username: regex});
        return user;
    }
    catch{
        return null;
    }
};

exports.getById = async(id) => {

    try{
        var user = await  User.findOne({_id: id});
        return user;
    }
    catch{
        return null;
    }
};

exports.createUser = async(userData) => {
    const user = new User(userData);
    return await user.save();
};

exports.list = async() => {

    try{
        var users = await User.find()
        users = users || [];
        return users;
    }
    catch{
        return [];
    }
};

exports.listLimit = async(perPage, page) => {

    try{
        var users = await User.find().limit(perPage).skip(perPage * page)
        users = users || [];
        return users;
    }
    catch{
        return [];
    }
};


exports.listByUsername = async(username_list) => {

    try{
        var users = await User.find({ username: { $in: username_list } });
        return users || [];
    }
    catch{
        return [];
    }
};

exports.save = async(user, modified_list) => {

    try{
        if(!user) return null;

        if(modified_list)
        {
            for (let i=0; i<modified_list.length; i++) {
                user.markModified(modified_list[i]);
            }
        }

        return await user.save();
    }
    catch{
        return null;
    }
};

exports.update = async(user, userData) => {

    try{
        if(!user) return null;

        for (let i in userData) {
            user[i] = userData[i];
            user.markModified(i);
        }

        var updatedUser = await user.save();
        return updatedUser;
    }
    catch{
        return null;
    }
};

exports.patchUser = async(userId, userData) => {

    try{
        var user = await User.findById ({_id: userId});
        if(!user) return null;

        for (let i in userData) {
            user[i] = userData[i];
        }

        var updatedUser = await user.save();
        return updatedUser;
    }
    catch{
        return null;
    }
};

exports.removeById = async(userId) => {

    try{
        var result = await User.deleteOne({_id: userId});
        return result && result.deletedCount > 0;
    }
    catch{
        return false;
    }
};

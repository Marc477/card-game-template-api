const UsersController = require('./users.controller');
const UsersCardsController = require("./users.cards.controller");
const UsersFriendsController = require("./users.friends.controller");
const UserTool = require('./users.tool');
const AuthTool = require('../authorization/auth.tool');
const config = require('../config');

const ADMIN = config.permissionLevels.ADMIN; //Highest permision, can read and write all users
const SERVER = config.permissionLevels.SERVER; //Middle permission, can read all users and grant rewards
const USER = config.permissionLevels.USER; //Lowest permision, can only do things on same user

exports.route = function (app) {
  app.post("/users/register", app.auth_limiter, [
    UsersController.registerUser,
  ]);
  app.get("/users", [
    AuthTool.isValidJWT,
    AuthTool.isPermissionLevel(USER),
    UsersController.getAll,
  ]);
  app.get("/users/:userId", [
    AuthTool.isValidJWT,
    AuthTool.isPermissionLevel(USER),
    UsersController.getUser,
  ]);
  app.post("/users/edit/:userId", app.post_limiter, [
    AuthTool.isValidJWT,
    AuthTool.isPermissionLevel(USER),
    AuthTool.isSameUserOr(ADMIN),
    UsersController.patchUserAccount,
  ]);
  app.post("/users/email/edit", app.auth_limiter, [
    AuthTool.isValidJWT,
    AuthTool.isPermissionLevel(USER),
    UsersController.patchEmail,
  ]);
  app.post("/users/password/edit", app.auth_limiter, [
    AuthTool.isValidJWT,
    AuthTool.isPermissionLevel(USER),
    UsersController.patchPassword,
  ]);
  app.post("/users/password/reset", app.auth_limiter, [
    UsersController.resetPassword,
  ]);
  app.post("/users/password/reset/confirm", app.auth_limiter, [
    UsersController.resetPasswordConfirm,
  ]);
  //app.delete("/users/:userId", app.post_limiter, [
  //  AuthTool.isValidJWT,
  //  AuthTool.permissionLevelRequired(ADMIN),
  //   UsersController.removeById,
  //]);
  
  // USER - EMAIL ---------------------------

  //Email confirm
  app.get("/users/email/confirm/:userId/:emailCode", [
    UsersController.confirmEmail,
  ]);

  app.post("/users/email/resend", app.auth_limiter, [
    AuthTool.isValidJWT,
    AuthTool.isPermissionLevel(USER),
    UsersController.resendEmail,
  ]);

  app.post("/users/email/send", app.auth_limiter, [
    AuthTool.isValidJWT,
    AuthTool.isPermissionLevel(ADMIN),
    UsersController.sendEmail,
  ]);

  // USER - STATS ---------------------------

  app.get("/online", [
    UsersController.getOnline
  ]);

  // USER - CARDS --------------------------------------

  app.post("/users/cards/add/:userId", app.post_limiter, [
    AuthTool.isValidJWT,
    AuthTool.isPermissionLevel(ADMIN),  
    AuthTool.isSameUserOr(ADMIN),
    UsersCardsController.AddCards,
  ]);

  //Decks
  app.post('/users/deck/:userId/:deckId', app.post_limiter, [
      AuthTool.isValidJWT,
      AuthTool.isPermissionLevel(USER),
      AuthTool.isSameUserOr(ADMIN),
      UsersCardsController.UpdateDeck
  ]);
  app.delete('/users/deck/:userId/:deckId', app.post_limiter, [
      AuthTool.isValidJWT,
      AuthTool.isPermissionLevel(USER),
      AuthTool.isSameUserOr(ADMIN),
      UsersCardsController.DeleteDeck
  ]);

  app.get("/users/stats/cards", [
    AuthTool.isValidJWT,
    AuthTool.isPermissionLevel(USER),
    UsersCardsController.getCardStats
  ]);

  // USER - Friends --------------------------------------

  app.post("/users/friends/add/:userId/:friendUser", app.post_limiter, [
    AuthTool.isValidJWT,
    AuthTool.isPermissionLevel(USER),
    AuthTool.isSameUserOr(ADMIN),
    UsersFriendsController.AddFriend,
  ]);

  app.post("/users/friends/remove/:userId/:friendUser", app.post_limiter, [
    AuthTool.isValidJWT,
    AuthTool.isPermissionLevel(USER),
    AuthTool.isSameUserOr(ADMIN),
    UsersFriendsController.RemoveFriend,
  ]);

  app.get("/users/friends/list/:userId/", [
    AuthTool.isValidJWT,
    AuthTool.isPermissionLevel(USER),
    AuthTool.isSameUserOr(ADMIN),
    UsersFriendsController.ListFriends,
  ]);

};
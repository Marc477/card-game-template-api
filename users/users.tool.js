const config = require('../config.js');
const crypto = require('crypto');
const Email = require('../tools/email.tool');
const CardTool = require('../tools/cards.tool');
const AuthTool = require('../authorization/auth.tool');

const UserTool = exports;

UserTool.generateID = function(length, easyRead) {
    var result           = '';
    var characters       = 'abcdefghijklmnopqrstuvwxyz0123456789';
    if(easyRead)
        characters  = 'abcdefghijklmnpqrstuvwxyz123456789'; //Remove confusing characters like 0 and O
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

 UserTool.sendEmailConfirmKey = (user, email, emailConfirmKey) => {

    if(!email || !user) return;

    var subject = config.api_title + " - Email Confirmation";
    var http = config.allow_https ? "https://" : "http://";
    var confirm_link = http + config.api_url + "/users/email/confirm/" + user.id + "/" + emailConfirmKey;

    var text = "Hello " + user.username + "<br>";
    text += "Welcome! <br><br>";
    text += "To confirm your email, click here: <br><a href='" + confirm_link + "'>" + confirm_link + "</a><br><br>";
    text += "Thank you and see you soon!<br>";

    Email.SendEmail(email, subject, text, function(result){
        console.log("Sent email to: " + email + ": " + result);
    });

};

UserTool.sendEmailChangeEmail = (user, email, new_email) => {

    if(!email || !user) return;

    var subject = config.api_title + " - Email Changed";

    var text = "Hello " + user.username + "<br>";
    text += "Your email was succesfully changed to: " + new_email + "<br>";
    text += "If you believe this is an error, please contact support immediately.<br><br>"
    text += "Thank you and see you soon!<br>";
    
    Email.SendEmail(email, subject, text, function(result){
        console.log("Sent email to: " + email + ": " + result);
    });
};

UserTool.sendEmailChangePassword = (user, email) => {

    if(!email || !user) return;

    var subject = config.api_title + " - Password Changed";

    var text = "Hello " + user.username + "<br>";
    text += "Your password was succesfully changed<br>";
    text += "If you believe this is an error, please contact support immediately.<br><br>"
    text += "Thank you and see you soon!<br>";

    Email.SendEmail(email, subject, text, function(result){
        console.log("Sent email to: " + email + ": " + result);
    });

};

UserTool.sendEmailPasswordRecovery = (user, email) => {

    if(!email || !user) return;

    var subject = config.api_title + " - Password Recovery";

    var text = "Hello " + user.username + "<br>";
    text += "Here is your password recovery code: " + user.passwordRecoveryKey.toUpperCase() + "<br><br>";
    text += "Thank you and see you soon!<br>";

    Email.SendEmail(email, subject, text, function(result){
        console.log("Sent email to: " + email + ": " + result);
    });
};

UserTool.setUserPassword = (user, password) =>
{
    let newPass = AuthTool.hashPassword(password);
    user.password = newPass;
    user.passwordRecoveryKey = ""; //After changing password, disable recovery until changed again
    user.refreshKey = crypto.randomBytes(16).toString('base64'); //Logout previous logins by changing the refreshKey
}

UserTool.changePassword = (user, password) =>
{
    if(password.length < 5 || password.length > 50)
        return false;

    let newPass = AuthTool.hashPassword(password);
    user.password = newPass;
    user.passwordRecoveryKey = ""; //After changing password, disable recovery until changed again
    user.refreshKey = crypto.randomBytes(16).toString('base64'); //Logout previous logins by changing the refreshKey
}

UserTool.addRegisterSession = function(user_ip)
{
    var value = register_sessions[user_ip] || 0;
    register_sessions[user_ip] = value + 1;
}

UserTool.clearRegisterSessions = function()
{
    register_sessions = {};
}

UserTool.hasCard = (cards, card_tid, quantity) =>
{
    for (let c = 0; c < cards.length; c++) {
        var acard = cards[c];
        var aquantity = acard.quantity || 1;
        if(acard.tid == card_tid && aquantity >= quantity)
            return true;
    }
    return false;
};

//newCards is just an array of string (card tid)
UserTool.addCards = async(user, newCards) =>
{
    var cards = user.cards;

    if(!Array.isArray(cards) || !Array.isArray(newCards))
        return false; //Wrong params

    if(newCards.length == 0)
        return true; //No card to add, succeeded

    //Count quantities
    let prevTotal = Validator.countQuantity(cards);
    let addTotal = Validator.countQuantity(newCards);

    //Loop on cards to add
    for (let c = 0; c < newCards.length; c++) {

        var cardAdd = newCards[c];
        var cardAddTid = typeof cardAdd === 'object' ? cardAdd.tid : cardAdd;
        var cardAddQ = typeof cardAdd === 'object' ? cardAdd.quantity : 1;

        if (cardAddTid) {
            var quantity = cardAddQ || 1; //default is 1
            var found = false;

            for (let i = 0; i < cards.length; i++) {
                if (cards[i].tid == cardAddTid) {
                    cards[i].quantity += quantity;
                    found = true;
                }
            }

            if (!found) {
                cards.push({
                    tid: cardAddTid,
                    quantity: quantity,
                });
            }
        }
    }

    //Remove empty
    for(var i=cards.length-1; i>=0; i--)
    {
        var card = cards[i];
        if(!card.quantity || card.quantity <= 0)
            cards.splice(i, 1);
    }

    //Validate quantities to make sure the array was updated correctly, this is to prevent users from loosing all their cards because of server error which would be terrible.
    var valid = Validator.validateArray(cards, prevTotal + addTotal);
    return valid;
};

UserTool.getDeck = (user, deck_tid) =>
{
    var deck = {};
    if(user && user.decks)
    {
        for(var i=0; i<user.decks.length; i++)
        {
            var adeck = user.decks[i];
            if(adeck.tid == deck_tid)
            {
                deck = adeck;
            }
        }
    }  
    return deck;
};

module.exports = UserTool;
const UserModel = require("./users.model");
const Activity = require("../activity/activity.model");
const UserTool = require('./users.tool');
const config = require('../config.js');

exports.AddFriend = async (req, res) => {

  const userId = req.params.userId;
  const friendUser = req.params.friendUser;

  //Validate params
  if (!friendUser || !userId) {
    return res.status(400).send({ error: "Invalid parameters" });
  }

  //Get the user
  const user = await UserModel.getById(userId);
  if (!user)
    return res.status(404).send({ error: "Can't find user " + userId });

  const friend = await UserModel.getByUsername(friendUser);
  if (!friend)
    return res.status(404).send({ error: "Can't find user " + friendUser });

  if(user.id == friend.id)
    return res.status(400).send({ error: "Can't add yourself" });
  
  var friends = user.friends || [];
  if(!friends.includes(friend.username))
    friends.push(friend.username);

  //Update the user array
  var update = { friends: friends };
  var updatedUser = await UserModel.patchUser(userId, update);
  if (!updatedUser) return res.status(400).send({ error: "Error updating user: " + userId });

  // Activity Log -------------
  const activityData =  {
      user: user.username,
      friend: friendUser,
  };
  
  //const a = await Activity.LogActivity("add_friend", req.jwt.username, activityData);
  //if (!a) return res.status(500).send({ error: "Failed to log activity!!" });

  // -------------
  return res.status(200).send(updatedUser.deleteSecrets());
};

exports.RemoveFriend = async(req, res) => {

  const userId = req.params.userId;
  const friendUser = req.params.friendUser;

  //Validate params
  if (!friendUser || !userId) {
    return res.status(400).send({ error: "Invalid parameters" });
  }

  //Get the user
  const user = await UserModel.getById(userId);
  if (!user)
    return res.status(404).send({ error: "Can't find user " + userId });

  var friends = user.friends || [];
  if(friends.includes(friendUser))
    friends.remove(friendUser);

  //Update the user array
  var update = { friends: friends };
  var updatedUser = await UserModel.patchUser(userId, update);
  if (!updatedUser) return res.status(400).send({ error: "Error updating user: " + userId });

  // Activity Log -------------
  const activityData =  {
      user: user.username,
      friend: friendUser,
  };
  
  //const a = await Activity.LogActivity("remove_friend", req.jwt.username, activityData);
  //if (!a) return res.status(500).send({ error: "Failed to log activity!!" });

  // -------------
  return res.status(200).send(updatedUser.deleteSecrets());

};

exports.ListFriends = async(req, res) => 
{

  const userId = req.params.userId;

  //Validate params
  if (!userId) {
    return res.status(400).send({ error: "Invalid parameters" });
  }

  //Get the user
  const user = await UserModel.getById(userId);
  if (!user)
    return res.status(404).send({ error: "Can't find user " + userId });

  var friends_users = user.friends || [];

  const friends = await UserModel.listByUsername(friends_users);
  if (!friends)
    return res.status(404).send({ error: "Can't find user friends " + userId });

  //Reduce visible fields
  for(var i=0; i<friends.length; i++)
  {
    friends[i] = {
      username: friends[i].username,
      rank: friends[i].rank,
      avatar: friends[i].avatar,
      xp: friends[i].xp,
      lastLoginTime: friends[i].lastLoginTime,
    }
  }

  return res.status(200).send({username: user.username, friends: friends, serverTime: new Date()});
  
}

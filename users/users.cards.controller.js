const UserModel = require("./users.model");
const UserTool = require("./users.tool");
const Validator = require("../tools/validator.tool");
const Activity = require("../activity/activity.model");
const config = require('../config');

exports.AddCards = async (req, res) => {

  if(!req.body.cards && req.body.card)
    req.body.cards = [req.body.card];

  const userId = req.params.userId;
  const cardsToAdd = req.body.cards;

  //Validate params
  if (!userId || typeof userId !== "string")
        return res.status(400).send({ error: "Invalid parameters" });

  if (!cardsToAdd || !Array.isArray(cardsToAdd))
    return res.status(400).send({ error: "Invalid parameters" });

  //Get the user add update the array
  const user = await UserModel.getById(userId);
  if (!user)
    return res.status(404).send({ error: "Cant find user " + userId });
  
  var validCards = UserTool.addCards(user, cardsToAdd);
  
  if (!validCards)
    return res.status(500).send({ error: "Error when adding cards " + userId });

  //Update the user array
  var update = { cards: user.cards };
  var updatedUser = await UserModel.update(user, update);
  if (!updatedUser) return res.status(500).send({ error: "Error updating user: " + userId });

  // Activity Log -------------
  const activityData =  {cards: user.cards, user: user.username};
  const act = await Activity.LogActivity("user_add_card", req.jwt.username, activityData);
  if (!act) return res.status(500).send({ error: "Failed to log activity!!" });

  // -------------
  return res.status(200).send(updatedUser.deleteSecrets());
};

exports.AddPackCards = async (req, res) => {

  const userId = req.params.userId;
  const quantity = req.body.quantity;

  //Validate params
  if (!userId || typeof userId !== "string")
      return res.status(400).send({ error: "Invalid parameters" });

  if(!quantity || !Number.isInteger(quantity))
      return res.status(400).json({error: "Invalid parameters"});

  //Get the user add update the array
  const user = await UserModel.getById(userId);
  if (!user)
    return res.status(404).send({ error: "Cant find user " + userId });

  var cardsToAdd = UserTool.getRandomCards(quantity);
  var validCards = UserTool.addCards(user, cardsToAdd);
  
  if (!validCards)
    return res.status(500).send({ error: "Error when adding cards " + userId });

  //Update the user array
  var update = { cards: user.cards };
  var updatedUser = await UserModel.update(user, update);
  if (!updatedUser) return res.status(500).send({ error: "Error updating user: " + userId });

  // Activity Log -------------
  const activityData =  {cards: user.cards, user: user.username};
  const act = await Activity.LogActivity("user_add_pack_card", req.jwt.username, activityData);
  if (!act) return res.status(500).send({ error: "Failed to log activity!!" });

  // -------------
  return res.status(200).send(cardsToAdd);
};


exports.FoilCard = async(req, res) =>
{
    const card_id = req.body.card;
    const userId = req.params.userId;

    if (!card_id || typeof card_id !== "string")
      return res.status(400).send({ error: "Invalid parameters" });
    if (!userId || typeof userId !== "string")
      return res.status(400).send({ error: "Invalid parameters" });

    if(card_id.endsWith("_foil"))
      return res.status(400).send({ error: "Can't foil a foil" });
    if(card_id.endsWith("_nft"))
      return res.status(400).send({ error: "Can't foil a collector" });

    //Get the user add update the array
    const user = await UserModel.getById(userId);
    if (!user)
      return res.status(404).send({ error: "Cant find user " + userId });

    if(!UserTool.hasCard(user.cards, card_id, 5))
      return res.status(403).send({ error: "Not enough cards"});

    var cardsToAdd = [{tid: card_id + "_foil", quantity: 1}];
    var cardsToRemove = [{tid: card_id, quantity: -5}];
    var validFoil = UserTool.addCards(user, cardsToAdd);
    var validCards = UserTool.addCards(user, cardsToRemove);

    if (!validCards || !validFoil)
      return res.status(500).send({ error: "Error when foiling cards " + userId });

     //Update the user array
     var update = { cards: user.cards };
     var updatedUser = await UserModel.update(user, update);
     if (!updatedUser) return res.status(500).send({ error: "Error updating user: " + userId });

    // Activity Log -------------
    const activityData =  {cards: user.cards, user: user.username};
    const act = await Activity.LogActivity("user_foil_card", req.jwt.username, activityData);
    if (!act) return res.status(500).send({ error: "Failed to log activity!!" });
    
    // -------------
    return res.status(200).send(updatedUser.deleteSecrets());
};

exports.UpdateDeck = async(req, res) => {

    if(!req.params.deckId || !req.params.userId)
        return res.status(400).json({error: "Invalid parameters"});

    var userId = req.params.userId;
    var deckId = req.params.deckId;

    var ndeck = {
        tid: req.params.deckId,
        title: req.body.title || "Deck",
        artefact: req.body.artefact || "",
        cards: req.body.cards || [],
    };

    var user = await UserModel.getById(userId);
    if(!user)
      return res.status(404).send({error: "User not found: " + userId});

    var decks = user.decks || [];
    var found = false;
    var index = 0;
    for(var i=0; i<decks.length; i++){
      var deck = decks[i];
      if(deck.tid == deckId)
      {
         decks[i]= ndeck;
         found = true;
         index = i;
      }
    }

    //Add new
    if(!found && ndeck.cards.length > 0)
      decks.push(ndeck);

    //Delete deck
    if(found && ndeck.cards.length == 0)
      decks.splice(index, 1);

    var userData = { decks: decks};
    var upUser = await UserModel.update(user, userData);
    if (!upUser) return res.status(500).send({ error: "Error updating user: " + userId });

    return res.status(200).send(upUser.deleteSecrets());
};

exports.DeleteDeck = async(req, res) => {

    if(!req.params.deckId || !req.params.userId)
        return res.status(400).json({error: "Invalid parameters"});

    var userId = req.params.userId;
    var deckId = req.params.deckId;

    var user = await UserModel.getById(userId);
    if(!user)
        return res.status(404).send({error: "User not found: " + userId});

    var decks = user.decks || {};
    var index = -1;
    for(var i=0; i<decks.length; i++){
      var deck = decks[i];
      if(deck.tid == deckId)
      {
        index = i;
      }
    }
    
    if(index >= 0)
      decks.splice(index, 1);

    var userData = { decks: decks};
    var upUser = await UserModel.update(user, userData);
    if (!upUser) return res.status(500).send({ error: "Error updating user: " + userId });

    return res.status(200).send(upUser);
};

exports.getCardStats = async(req, res) =>
{
    var users = await UserModel.list();

    var cards = new Map();

    users.forEach((user, index) =>
    {
      if(user.username != "Dealer")
      {
        user.cards.forEach((card, cindex) =>
        {
            AddCardStats(cards, card)
        });
      }
    });

    var array = Array.from(cards, ([name, value]) => (value));
    array.sort((a, b) =>
    {
        if(a.quantity == b.quantity)
            return a.tid.localeCompare(b.tid);
        return a.quantity > b.quantity ? -1 : 1;
    });

    return res.status(200).send(array);
}

var AddCardStats = function(cards, card)
{
    if(card)
    {
        if(card.tid.endsWith("_foil"))
            card.tid = card.tid.replace("_foil", "");
        if(card.tid.endsWith("_nft"))
            card.tid = card.tid.replace("_nft", "");

        var data;
        if(cards.has(card.tid))
        {
            data = cards.get(card.tid);
        }
        else
        {
            data = {
                tid: card.tid,
                rarity: "",
                quantity: 0,
            };

            if(config.cards_common.includes(card.tid))
              data.rarity = "common";
            if(config.cards_uncommon.includes(card.tid))
              data.rarity = "uncommon";
            if(config.cards_rare.includes(card.tid))
              data.rarity = "rare";
            if(config.cards_mythic.includes(card.tid))
              data.rarity = "mythic";
        }

        data.quantity += card.quantity;

        cards.set(card.tid, data);
    }
}

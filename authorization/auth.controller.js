
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const config = require('../config.js');
const jwtSecret = config.jwt_secret;
const UserModel = require('../users/users.model');
const UserTool = require('../users/users.tool');

exports.login = (req, res) => {
    try {
        let refreshId = req.login.userId + jwtSecret;
        let salt = crypto.randomBytes(16).toString('base64');
        let hash = crypto.createHmac('sha512', salt).update(refreshId).digest("base64");
        req.login.refreshKey = salt;

        let token = jwt.sign(req.login, jwtSecret);
        let b = Buffer.from(hash);
        let refresh_token = b.toString('base64');

        //Delete some keys for security, empty keys are never valid
        UserModel.patchUser(req.login.userId, {refreshKey: salt, proofKey: "", passwordRecoveryKey: "", lastLoginTime: new Date()});

        res.status(201).send({id: req.login.userId, username: req.login.username, accessToken: token, refreshToken: refresh_token, version: config.version});
    } catch (err) {
        res.status(500).send({error: err});
    }
};

exports.get_version = (req, res) =>{
    res.status(201).send({version: config.version});
};

// ----- verify user -----------

exports.validatetoken = async(req, res, next) => {
    
    var token = req.jwt;
    var data = {id: token.userId, username: token.username, loginTime: new Date(token.iat * 1000), serverTime: new Date() };
    return res.status(200).send(data);
};

exports.createProof = async(req, res) =>
{
    var userId = req.jwt.userId;

    var user = await UserModel.getById(userId);
    if(!user)
        return res.status(404).send({error: "User not found"});

    user.proofKey = crypto.randomBytes(20).toString('base64');
    await UserModel.save(user);

    return res.status(200).send({proof: user.proofKey});
}

exports.validateProof = async(req, res) =>
{
    var username = req.params.username;
    var proof = req.params.proof;

    if(!username || typeof username != "string" || !proof || typeof proof != "string")
        return res.status(400).json({error: "Invalid parameters"});

    var user = await UserModel.getByUsername(username);
    if(!user)
        return res.status(404).send({error: "User not found"});
    
    if(!user.proofKey || user.proofKey != proof)
        return res.status(403).send({error: "Invalid Proof"});

    return res.status(200).send();
}